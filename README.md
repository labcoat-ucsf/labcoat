# LabCOAT #



### What is LabCOAT? ###

**LabCOAT** (**Lab**oratory **C**oncepts **O**rganized **A**s **T**echnology)is a science and medical technology company with research & development, AI/ML data analytics, IoT, and insurtech divisions.

**LabCOAT** is a private blockchain network comprised of digital healthcare insurance contracts optimized for coverage and cost efficiency; secure EHRs with the option to provide anonymous patient data in exchange for redeemable tokens; IoT networks linking medical devices; and an AI/ML data analytics platform for predictive diagnosis and preventive medicine.